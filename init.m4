#!/bin/sh
#
readonly LAST_MODIFY='2021.10.03'
readonly AUTHOR='Petar Toshev'
readonly VERSION='2.1'
readonly PROGRAM_NAME='Continious Deployer'
readonly DESCRIPTION="
        ${PROGRAM_NAME} is script for helping deploying project via CD
        Version: ${VERSION}
        Created by ${AUTHOR}
        Last modification: ${LAST_MODIFY}
"
#########################################################################
## Parameters
#########################################################################

# ARG_HELP([${DESCRIPTION}])
# ARG_VERSION([echo ${PROGRAM_NAME} ${VERSION}])
# ARG_OPTIONAL_BOOLEAN([verbose], [x], [Turn on verbose mode])
#
# ARG_OPTIONAL_SINGLE([check-interval], [i], [Check for new version in seconds], [60])
# ARG_TYPE_GROUP([pint], [COUNT_CHECK_INTERVAL], [check-interval])
# ARG_OPTIONAL_SINGLE([deploy-dir], [d], [Deploy new versions in], [/deployed])
# ARG_OPTIONAL_SINGLE([src-dir], [s], [Where to search for new version], [/versions])
# ARG_OPTIONAL_SINGLE([log-file], [l], [Where to write logs of the script], [/logs/deploy])
# ARG_OPTIONAL_SINGLE([group-id], [g], [Id of group of the files])
# ARG_TYPE_GROUP([nnint], [COUNT_GROUP_ID], [group-id])
# ARG_OPTIONAL_SINGLE([checksum-wait-time], [c], [Wait interval between checksums], [10])
# ARG_OPTIONAL_SINGLE([max-old-versions], [m], [Delete version aftre N new vertsions])
# ARG_TYPE_GROUP([pint], [COUNT_MAX_OLD_VERSIONS], [max-old-versions])
#
# ARG_OPTIONAL_BOOLEAN([enable-java], [j], [Enable java server (copy jar)])
# ARG_OPTIONAL_BOOLEAN([remove-log], [r], [Remove log file on startup])
# ARG_OPTIONAL_SINGLE([copy-dir-name-src], [f], [Copy folder from version])
# ARG_OPTIONAL_SINGLE([copy-dir-name-dst], [t], [Copy folder from version to (Default is same as copy-dir-name-src]))
#
#
# ARGBASH_GO

# [ <-- needed because of Argbash

#########################################################################
# Variables
#########################################################################

# Fix default value
_arg_copy_dir_name_dst="${_arg_copy_dir_name_dst:-${_arg_copy_dir_name_src}}"

readonly VERSION_FILE=${_arg_deploy_dir}/version
readonly JAR=${_arg_deploy_dir}/app.jar

#########################################################################
# Functions
#########################################################################

print_to_file() {
  printf "$* \n" | tee -a ${_arg_log_file}
}

log_helper() {
  print_to_file "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*"
}

debug() {
  if enabled "${_arg_verbose}"; then
    log_helper "DEBUG    $*"
  fi
}

log() {
  log_helper "INFO    $*"
}

err() {
  log_helper "ERROR   $*" 1>&2
}

current_version() {
  cat ${VERSION_FILE} 2>/dev/null
}

list_all_versions() {
  ls -pt ${_arg_src_dir} | grep / | sed 's/\///'
}

last_version() {
  list_all_versions | head -n 1
}

calculate_md5sum() {
  find $1 -type f -exec md5sum {} \; | sort -k 2 | md5sum | awk '{print $1}'
}

last_modified() {
  stat -c %Y $1 2>/dev/null
}

have_to_change_with() {
  candidate="$(last_version)"
  if [[ ! -e "${VERSION_FILE}" ]] \
     || [[ "$(last_modified ${_arg_deploy_dir})" -lt "$(last_modified ${_arg_src_dir}/${candidate})" \
          && "${candidate}" != "$(current_version)" ]]; then

    echo "${candidate}"

  fi
}

swap_version_sum_helper() {
  calculate_md5sum ${_arg_src_dir}/${1}
}

swap_versions() {
  # recieves new version as $1
  log Will swap version with ${1}
  log Starting checksum check

  checksum1="$(swap_version_sum_helper ${1})"
  sleep ${_arg_checksum_wait_time}
  checksum2="$(swap_version_sum_helper ${1})"

  while [[ "${checksum1}" != "${checksum2}" ]]; do
    log "Versions did not match... ${checksum1} != ${checksum2}"
    checksum1="${checksum2}"
    sleep ${_arg_checksum_wait_time}
    checksum2="$(swap_version_sum_helper ${1})"
  done

  log "Sum matched ${checksum1}"
  stop_servers
  swap_data "${1}"
  start_servers
  log Swapped versions
}

enabled() {
  [[ "$1" == 'on' ]]
  return $?
}

swap_data() {
  # new version is in $1
  log Swapping data started

  log Removing old data
  rm -rf ${JAR}
  rm -rf ${_arg_deploy_dir}/${_arg_copy_dir_name_dst}/*

  log Copying new data
  if enabled "${_arg_enable_java}"; then
    log Copying jar ${_arg_src_dir}/${1}/*.jar to ${JAR}
    cp ${_arg_src_dir}/${1}/*.jar ${JAR}
  fi

  if [[ -n "${_arg_copy_dir_name_src}" ]]; then
    log Copying dir ${_arg_src_dir}/${1}/${_arg_copy_dir_name_src}/* into ${_arg_deploy_dir}/${_arg_copy_dir_name_dst}/
    cp -r ${_arg_src_dir}/${1}/${_arg_copy_dir_name_src}/* ${_arg_deploy_dir}/${_arg_copy_dir_name_dst}/
  fi

  log Updating version file
  echo "$1" > ${VERSION_FILE}

  if [[ -n "${_arg_group_id}" ]]; then
    log Fixing group permissions
    chgrp -RL ${_arg_group_id} ${_arg_deploy_dir}
  fi

  if [[ -n "${_arg_max_old_versions}" ]]; then
    log Deleting old versions
    number_of_current_versions=$(list_all_versions | wc -l)
    number_of_versions_to_remove=$(( ${number_of_current_versions} - ${_arg_max_old_versions} ))
    log Found ${number_of_current_versions} versions. Should remove ${number_of_versions_to_remove}
    if [[ "${number_of_versions_to_remove}" -gt 0 ]]; then 
      for i in $(list_all_versions | tail -n +${_arg_max_old_versions}); do
        log Removing ${i}
        rm -rf ${_arg_src_dir}/${i}
      done
    fi
  fi

  log Swapping data finished
}

start_servers() {
  log Starting servers
  if enabled "${_arg_enable_java}" && [[ -z "${server_java_pid}"  ]]; then
    log Starting java server with app version $(current_version)
    java -jar ${JAR} &
    server_java_pid=$!
    log Started java server with pid ${server_java_pid}
  fi
  log Started server ended
}

stop_servers() {
  log Stopping servers
  if enabled "${_arg_enable_java}" && [[ -n "${server_java_pid}"  ]]; then
    log Killing java server on pid ${server_java_pid}
    kill -9 ${server_java_pid}
    log Killed pid ${server_java_pid}
    unset server_java_pid
  fi
  log Servers stopped
}

check_working() {
  log Check are servers active
}

#########################################################################
# Main
#########################################################################

log Starting script version ${VERSION}. Last updated on ${LAST_MODIFY}

mkdir -p "${_arg_deploy_dir}"
mkdir -p "$(dirname ${_arg_log_file})"

if enabled "${_arg_remove_log}"; then
  rm -f "${_arg_log_file}"
fi

new_version="$(have_to_change_with)"
if [[ -n "${new_version}" ]]; then
  swap_versions "${new_version}"
else
  start_servers
fi

while true; do
  sleep ${_arg_check_interval}
  new_version="$(have_to_change_with)"
  debug last version ${new_version}
  if [[ -n "${new_version}" ]]; then
    swap_versions "${new_version}"
  fi
done
  
# ] <-- needed because of Argbash
